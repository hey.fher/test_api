const validate = require('validate')

/**
 * Validador del objeto body
 *
 * @param  { Object } body   Documento req.body
 * @param  { Object } schema Esquema de validacion
 * @return { Boolean | Array } Devuelve un array con los errores si es que los hay, si no retorna false
 */
module.exports = (body = {}, schema) => {
	let data = new validate(schema)
    let errors = data.validate(body)

    if (errors.length) {
        return errors.map(item => {
	        return {
	            field: item.path,
	            message: item.message
	        }
	    })
    }

    return true
}
