const mongojs = require('mongojs')

class Database {
    constructor() {
        this.user = process.env.MUSR
        this.password = process.env.MPAS
        this.host = process.env.MHOS
        this.options = process.env.MOPT

        return this._connect()
    }

    _connect() {
        return mongojs(`mongodb://${ this.user }:${ this.password }@${ this.host }?${ this.options }`)
    }
}


module.exports = new Database()
