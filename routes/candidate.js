const express = require('express')
const router = express.Router()

router.post('/', require('./../controllers/createCandidate'))
router.get('/', require('./../controllers/getCandidates'))
router.put('/', require('./../controllers/updateCandidate'))
router.delete('/', require('./../controllers/deleteCandidate'))

module.exports = router
