const express = require('express')
const router = express()

router.use('/candidate', require('./candidate'))

module.exports = router
