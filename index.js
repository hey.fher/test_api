require('dotenv').config()
require('console-stamp')(console, { pattern: 'HH:MM:ss', label: false })

const cors = require('cors')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const routes = require('./routes')
const db = require('./config/db')

const express = require('express')
const app = express()
const port  = process.env.PORT



/**
 * Middlewares
 */
app.use(morgan('[ :method ] [ :status ] :url'))
app.use(cookieParser())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cors({
    origin: [
		'http://localhost:8080',
		'https://test-laboral.netlify.com'
	],
    credentials: true
}))



/**
 * Routes
 */
app.use('/api', routes)



/**
 * Server listening
 */
app.listen(port, () => {
    console.log(`[express] Server running at http://localhost:${ port }`)
})
