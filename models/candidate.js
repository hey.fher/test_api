const Schema = require('validate')
const validate = require('./../utils/validate')

module.exports = body => {
	return validate(body, {
		name: {
			type: String,
			required: true,
			length: { max: 20 }
		},

		last_name: {
			type: String,
			required: true,
			length: { max: 20 }
		},

		phone: {
			type: String,
			required: true,
			length: { max: 20 }
		},

		address: {
			type: String,
			required: true,
			length: { max: 50 }
		},

		laboral_references: {
			type: Array,
			each: {
				_id: {
					type: String,
					required: true
				},

				company: {
					type: String,
					required: true
				},

				contact: {
					type: String,
					required: true
				},

				start: {
					type: String,
					required: true
				},

				leave: {
					type: String
				}
			}
		}
	})
}
