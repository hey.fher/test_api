const candidateSchema = require('./../models/candidate')
const db = require('./../config/db')

module.exports = (req, res) => {
	let body = req.body
	let validatedSchema = candidateSchema(req.body)

	if (typeof validatedSchema === 'object') {
		return res.json({
			success: false,
			type: 'schema',
			errors: validatedSchema
		})
	}

	db.candidates.insert(body, (error, payload) => {
		if (error) {
			return res.json({
				success: false,
				type: 'database'
			})
		}

		res.json({
			payload,
			success: true
		})
	})
}
