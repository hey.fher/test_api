const db = require('./../config/db')

module.exports = (req, res) => {
	let { search } = req.query
	let searchExpression = new RegExp(`.*${ search }.*`, 'gi')

	db.candidates.aggregate([{
		$addFields: {
			full_name: {
				$concat: ['$name', ' ', '$last_name']
			}
		}
	}, {
		$match: {
			...search && { full_name: searchExpression }
		}
	}, {
		$sort: {
			_id: -1
		}
	}], (error, payload) => {
		if (error) {
			return res.json({
				success: false,
				type: 'database'
			})
		}

		res.json({
			payload,
			success: true
		})
	})
}
