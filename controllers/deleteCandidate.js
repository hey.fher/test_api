const { ObjectID } = require('mongojs')
const db = require('./../config/db')
const validate = require('./../utils/validate')

module.exports = (req, res) => {
	let validatedSchema = validate(req.query, {
		_id: {
			use: {
				mongo: v => ObjectID.isValid(v)
			},
			message: {
				mongo: `This document doesn't exist or was deleted.`
			}
		}
	})

	if (typeof validatedSchema === 'object') {
		return res.json({
			success: false,
			type: 'schema',
			errors: validatedSchema
		})
	}

	db.candidates.findAndModify({
		query: {
			_id: ObjectID(req.query._id)
		},
		remove: true,
		fields: {
			_id: 1
		},
	}, (error, payload) => {
		if (error) {
			return res.json({
				success: false,
				type: 'database'
			})
		}

		if (!payload) {
			return res.json({
				success: false,
				type: 'database',
				errors: [{ field: '_id', message: `This document doesn't exist or was deleted.` }]
			})
		}

		res.json({
			payload,
			success: true
		})
	})
}
