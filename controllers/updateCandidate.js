const { ObjectID } = require('mongojs')
const candidateSchema = require('./../models/candidate')
const db = require('./../config/db')
const validate = require('./../utils/validate')

module.exports = (req, res) => {
	let validateQuery = validate(req.query, {
		_id: {
			use: {
				mongo: v => ObjectID.isValid(v)
			},
			message: {
				mongo: `This document doesn't exist or was deleted.`
			}
		}
	})

	if (typeof validateQuery === 'object') {
		return res.json({
			success: false,
			type: 'schema',
			errors: validateQuery
		})
	}

	let validateBody = candidateSchema(req.body)

	if (typeof validateBody === 'object') {
		return res.json({
			success: false,
			type: 'schema',
			errors: validateBody
		})
	}

	db.candidates.findAndModify({
		query: {
			_id: ObjectID(req.query._id)
		},
		update: req.body,
		new: true
	}, (error, payload) => {
		if (error) {
			return res.json({
				success: false,
				type: 'database'
			})
		}

		if (!payload) {
			return res.json({
				success: false,
				type: 'database',
				errors: [{ field: '_id', message: `This document doesn't exist or was deleted.` }]
			})
		}
		
		res.json({
			payload,
			success: true
		})
	})
}
